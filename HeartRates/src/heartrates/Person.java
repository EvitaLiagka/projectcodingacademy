/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heartrates;

/**
 *
 * @author Home5536
 */
public class Person {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
       HeartRates person1 = new HeartRates("Koukou", "Roukou", 20, 9, 1994); 
       
        System.out.println("Personal details: " + person1.toString());
        System.out.println("Your age is: " + person1.calculateAge(1994));
        System.out.println("Your maximum heart rate is: " + person1.calculateMaximumHeartRate(25));
        person1.calculateTargetHeartRateRange(195);
    }
    
}
