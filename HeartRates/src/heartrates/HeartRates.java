/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heartrates;

/**
 *
 * @author Home5536
 */
import java.util.Calendar;

public class HeartRates {
    
   private String firstName;
   private String lastName;
   private int dayOfBirth;
   private int monthOfBirth;
   private int yearOfBirth;

    public HeartRates() {
    }
   

    public HeartRates(String firstName, String lastName, int dayOfBirth, int monthOfBirth, int yearOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dayOfBirth = dayOfBirth;
        this.monthOfBirth = monthOfBirth;
        this.yearOfBirth = yearOfBirth;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getDayOfBirth() {
        return dayOfBirth;
    }

    public void setDayOfBirth(int dayOfBirth) {
        this.dayOfBirth = dayOfBirth;
    }

    public int getMonthOfBirth() {
        return monthOfBirth;
    }

    public void setMonthOfBirth(int monthOfBirth) {
        this.monthOfBirth = monthOfBirth;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    @Override
    public String toString() {
        return "HeartRates{" + "firstName=" + firstName + ", lastName=" + lastName + ", dayOfBirth=" + dayOfBirth + ", monthOfBirth=" + monthOfBirth + ", yearOfBirth=" + yearOfBirth + '}';
    }
    

    public int calculateAge (int yearOfBirth){
        
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int currentMonth = Calendar.getInstance().get(Calendar.MONTH);
        int age = 0;
        
        if(monthOfBirth >=  currentMonth){
        age = (currentYear - yearOfBirth) -1;}
        else{
        age =  currentYear - yearOfBirth;
        }
        return age;
        }
    
    public int calculateMaximumHeartRate (int age){
        
        int beatsPerMinute =  220 - age;
        return beatsPerMinute;
        
    }
   
    public void calculateTargetHeartRateRange (int beatsPerMinute){
        
        double targetHeartRate1 = beatsPerMinute * 0.6;
        double targetHeartRate2 = beatsPerMinute * 0.7;
        double targetHeartRate3 = beatsPerMinute * 0.8;
        
        System.out.println("Your heart rate zones are: ");
        System.out.println("Zone 1:" + targetHeartRate1);
        System.out.println("Zone 2:" + targetHeartRate2);
        System.out.println("Zone 3:" + targetHeartRate3);
    }
}
