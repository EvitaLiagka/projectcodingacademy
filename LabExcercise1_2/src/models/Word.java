/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Home5536
 */
public class Word {

    String word;
    boolean pageFound;

    public Word() {
    }

    public Word(String word, boolean pageFound) {
        this.word = word;
        this.pageFound = pageFound;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public boolean isPageFound() {
        return pageFound;
    }

    public void setPageFound(boolean pageFound) {
        this.pageFound = pageFound;
    }

    @Override
    public String toString() {
        return "Word{" + "word=" + word + ", pageFound=" + pageFound + '}';
    }
    
    
    
}
