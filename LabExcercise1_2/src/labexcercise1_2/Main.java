/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labexcercise1_2;

import services.MSWordTextFinder;
import java.util.ArrayList;
import java.util.List;
import models.Word;
import services.SearchResults;

/**
 *
 * @author Home5536
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
     
     SearchResults searcher = new SearchResults();
     searcher.searchAndPrintResults();
     
    }
    
}
