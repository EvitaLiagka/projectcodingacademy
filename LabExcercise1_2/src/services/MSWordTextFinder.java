/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import models.Word;

/**
 *
 * @author Home5536
 */
public class MSWordTextFinder {

    public void findTextInWordDocument(String filePath, List<String> words) {

        try (XWPFDocument file = new XWPFDocument(new FileInputStream("C:\\Users\\Home5536\\Desktop\\Class Excercises\\Example.docx"));){

            XWPFWordExtractor wordExtractor = new XWPFWordExtractor(file);

            for (int i = 0; i < words.size(); i++) {

                System.out.println("Word " + words.get(i) + " exists in text: " + wordExtractor.getText().contains(words.get(i)));
                
//Na do giati otan vazo getters anti gia full-hardcoded de mou pairnei to equals alla pairnei to contains
                
            }

        } catch (IOException e) {

            System.out.println("File not found. Please, try again.");
        }
    }
}
