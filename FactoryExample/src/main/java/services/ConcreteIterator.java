/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.util.ArrayList;
import java.util.List;
import models.AbstractProduct;
import models.Product;
import models.Service;
import models.Subscription;


/**
 *
 * @author evita
 */
public class ConcreteIterator implements Iterator {

    public void listProductsByType(List<AbstractProduct> products) {
        
        List<Product> productList = new ArrayList<>();
        List<Service> serviceList = new ArrayList<>();
        List<Subscription> subscriptionList = new ArrayList<>();

        //na do kai forEach me lamda expr
        
        for (AbstractProduct product : products) {
            if (product instanceof Product) {
                productList.add((Product) product);
            } else if (product instanceof Service) {
                serviceList.add((Service) product);
            } else if (product instanceof Subscription){
                subscriptionList.add((Subscription) product);
            } else {
                System.out.println("Nothing was added to the list");
            }
        }System.out.println(productList.toString() + serviceList.toString() + subscriptionList.toString());
    }
}

