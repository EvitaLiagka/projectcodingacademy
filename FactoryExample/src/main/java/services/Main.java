/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.util.ArrayList;
import java.util.List;
import models.AbstractProduct;
import models.ProductCreator;
import models.TypeOfProduct;

/**
 *
 * @author evita
 */
public class Main {
     public static void main(String[] args) {
     ProductCreator creator = new ProductCreator();
   
     List<AbstractProduct> products = new ArrayList<>();
     AbstractProduct newProduct = creator.productFactory(TypeOfProduct.SUBSCRIPTION);
     products.add(newProduct);
     System.out.println(products);
     ConcreteIterator ci = new ConcreteIterator();
     ci.listProductsByType(products);
     
     }
}
