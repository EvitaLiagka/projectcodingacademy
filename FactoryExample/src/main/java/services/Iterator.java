/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.util.ArrayList;
import java.util.List;
import models.AbstractProduct;

/**
 *
 * @author evita
 */
public interface Iterator {
    
     public void listProductsByType(List<AbstractProduct> products);
}
