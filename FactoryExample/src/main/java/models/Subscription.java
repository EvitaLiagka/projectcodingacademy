/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.Timestamp;
import lombok.Builder;
import lombok.ToString;


/**
 *
 * @author evita
 */
@ToString(callSuper = true)
public class Subscription extends AbstractProduct {

    private Timestamp fromDate;
    private Timestamp toDate;
    
    @Builder
    public Subscription(){
        super ();
    
    }
}
