/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


/**
 *
 * @author evita
 */

@NoArgsConstructor
@Getter
@Setter
@ToString
public class AbstractProduct {

    private long id;
    private String name;
    private double price;
}
