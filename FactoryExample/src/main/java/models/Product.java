/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;



/**
 *
 * @author evita
 */

@ToString(callSuper=true)
public class Product extends AbstractProduct {

    private int size;
    private String provider;

    @Builder
    public Product() {
        super();
    }
    
    
}
