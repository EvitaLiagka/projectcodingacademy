/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Scanner;

/**
 *
 * @author evita
 */
public class ProductCreator {

    public AbstractProduct productFactory(TypeOfProduct typeOfProduct) {


        switch (typeOfProduct) {
            case PRODUCT:
                return new Product();
            case SERVICE:
                return new Service();
            case SUBSCRIPTION:
                return new Subscription();
            default:
                return null;

        }
    }
}
