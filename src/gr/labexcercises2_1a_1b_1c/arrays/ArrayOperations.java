package gr.labexcercises2_1a_1b_1c.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author evita
 */
public class ArrayOperations {

    /**
     * This is a method that finds a pair of two numbers, whose sum corresponds
     * to the given sum, in an array.
     *
     * @param array
     * @param sum
     */
    public void findPairSum(int array[], int sum) {

        Arrays.sort(array);
        int candidateNumber1 = 0;
        int candidateNumber2 = array.length - 1;

        while (candidateNumber1 < candidateNumber2) {
            if (array[candidateNumber1] + array[candidateNumber2] == sum) {
                System.out.println("Pair is consisted of numbers " + array[candidateNumber1] + " and " + array[candidateNumber2]);
                break;
            } else if (array[candidateNumber1] + array[candidateNumber2] < sum) {
                candidateNumber1++;
            } else {
                candidateNumber2--;
            }
        }
    }

    /**
     * This is a method that finds the index of a target number in an array,
     * using linear search.
     *
     * @param array
     * @param target
     */
    public void findIndexByLinearSearch(int array[], int target) {

        if (array == null) {
            System.out.println("Array is empty.");
        }

        int i = 0;

        while (i < array.length) {

            if (array[i] == target) {
                System.out.println("Target number's index is: " + i);
                break;
            } else if (array[i] == -1) {
                System.out.println("No such target number found.");
            } else {
                i++;
            }

        }

    }

    /**
     * This is a method that finds the index of a target number in an array,
     * using binary search.
     *
     * @param array
     * @param target
     */
    public void findIndexByBinarySearch(int array[], int target) {

        int index = Arrays.binarySearch(array, target);

        if (index == -1) {
            System.out.println("No such target number found.");
        } else {
            System.out.println("Target number's index is: " + index);
        }
    }

    /**
     * This is a method that finds the number of occurences of a target number
     * in an array.
     *
     * @param array
     * @param target
     */
    public void findOccurences(int array[], int target) {

        List<Integer> indexes = new ArrayList<>();

        for (int i = 0; i < array.length; i++) {
            if (array[i] == target) {
                indexes.add(i);
            }
        }
        System.out.println("Element " + target + " occurs " + indexes.size() + " times.");
    }
}
