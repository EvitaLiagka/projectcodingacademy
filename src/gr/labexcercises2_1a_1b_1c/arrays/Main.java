/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.labexcercises2_1a_1b_1c.arrays;

/**
 *
 * @author evita
 */
public class Main {

    /**
     * This is the main method of our application.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] Arr1 = {8, 7, 2, 5, 3, 1};
        int sum = 10;

        ArrayOperations ar = new ArrayOperations();
        ar.findPairSum(Arr1, sum);

        int[] Arr2 = {2, 3, 5, 7, 9};
        int target = 5;
        ar.findIndexByLinearSearch(Arr2, target);
        ar.findIndexByBinarySearch(Arr2, target);

        int[] Arr3 = {2, 5, 5, 5, 5, 8, 9, 9, 9};
        ar.findOccurences(Arr3, target);
    }

}
