/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.labexcercises2_2.streamingapi;

import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author evita
 */
public class StringsListOperations {

    /**
     * This is a method for filtering out all empty strings from a list of
     * strings by using streaming API
     *
     * @param originalList
     */
    public void manipulateStringList(List<String> originalList) {

        List<String> filteredList = originalList.stream().filter(str -> !str.isEmpty()).collect(Collectors.toList());
        System.out.println("After having been filtered the new list is: " + filteredList);
    }
}
