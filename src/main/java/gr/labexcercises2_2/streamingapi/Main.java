/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.labexcercises2_2.streamingapi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 *
 * @author evita
 */
public class Main {

    /**
     * This is the main method of the application
     *
     * @param args
     */
    public static void main(String[] args) {

        IntegersListOperations integersList = new IntegersListOperations();

        List<Integer> numbers = new ArrayList<>();
        Random randomizer = new Random();

        for (int i = 0; i <= 1000; i++) {
            numbers.add(randomizer.nextInt());
        }

        System.out.println("The min value is: " + integersList.findMinValue(numbers));
        System.out.println("The max value is: " + integersList.findMaxValue(numbers));
        System.out.println("The sum is: " + integersList.getSum(numbers));
        System.out.println("The average is: " + integersList.getAverage(numbers));
        System.out.println("The even numbers in the list are: " + integersList.findEvenNumbers(numbers));
        System.out.println("The odd numbers in the list are: " + integersList.findOddNumbers(numbers));
        

        StringsListOperations stringsList = new StringsListOperations();

        List<String> originalList = Arrays.asList("abc", "", "efg", "drdvs", "", "182", "");

        stringsList.manipulateStringList(originalList);
    }

}
