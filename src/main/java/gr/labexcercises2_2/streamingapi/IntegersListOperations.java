/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.labexcercises2_2.streamingapi;

import java.util.List;
import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.stream.Collectors;

/**
 *
 * @author evita
 */
public class IntegersListOperations {

    /**
     * This is a method for finding the minimum value in a list of integers by
     * using stream API
     *
     * @param numbers
     * @return minValue
     */
    public OptionalInt findMinValue(List<Integer> numbers) {

        OptionalInt minValue = numbers.stream().mapToInt(i -> i).min();
        return minValue;
    }

    /**
     * This is a method for finding the maximum value in a list of integers by
     * using stream API
     *
     * @param numbers
     * @return maxValue
     */
    public OptionalInt findMaxValue(List<Integer> numbers) {

        OptionalInt maxValue = numbers.stream().mapToInt(i -> i).max();
        return maxValue;
    }

    /**
     * This is a method for calculating the sum of a list of integers by using
     * stream API
     *
     * @param numbers
     * @return sum
     */
    public int getSum(List<Integer> numbers) {

        int sum = numbers.stream().mapToInt(i -> i).sum();
        return sum;
    }

    /**
     * This is a method for retrieving the average value from a list of integers
     * by using stream API
     *
     * @param numbers
     * @return average
     */
    public OptionalDouble getAverage(List<Integer> numbers) {

        OptionalDouble average = numbers.stream().mapToInt(i -> i).average();
        return average;
    }

    /**
     * This is a method for finding the even numbers in a list of integers by
     * using stream API
     *
     * @param numbers
     * @return evenNumbers
     */
    public List<Integer> findEvenNumbers(List<Integer> numbers) {
        List<Integer> evenNumbers = numbers.stream().map(i -> Integer.valueOf(i))
                .filter(i -> i % 2 == 0)
                .collect(Collectors.toList());
        return evenNumbers;
    }

    /**
     * This is a method for finding the odd numbers in a list of integers by
     * using stream API
     *
     * @param numbers
     * @return oddNumbers
     */
    public List<Integer> findOddNumbers(List<Integer> numbers) {
        List<Integer> oddNumbers = numbers.stream().map(i -> Integer.valueOf(i))
                .filter(i -> i % 2 != 0)
                .collect(Collectors.toList());
        return oddNumbers;
    }
}
