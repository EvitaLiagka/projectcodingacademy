/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author evita
 */
//@Entity oxi akoma giati den exoume vasi
@Data
@AllArgsConstructor

public class Employee {

    private long id;
    private String name;
    private int age;
    private double salary;

}
