/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import models.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import services.EmployeeServiceImpl;
import utils.EmployeeError;

/**
 *
 * @author evita
 */
@RestController
@Slf4j
public class RestApiController {

    @Autowired
    EmployeeServiceImpl employeeService;

    //@Autowired
    //an instance
    @RequestMapping(value = "/testme", method = RequestMethod.GET)
    public ResponseEntity<String> testme() {
        String myreturn = "hi";
        return new ResponseEntity<String>(myreturn, HttpStatus.OK);

    }

    @RequestMapping(value = "/employee", method = RequestMethod.GET)
    public ResponseEntity<List<Employee>> listAllUsers() {
        List<Employee> employees = employeeService.findAllEmployees();
        if (employees.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Employee>>(employees, HttpStatus.OK);
    }

    @RequestMapping(value = "/employee/{id}", method = RequestMethod.GET)
	   public ResponseEntity<?> getUser(@PathVariable("id") int id) {
	       log.info("Fetching & Deleting Employee with id {}", id);

	       Employee employee = employeeService.findById(id);
	       if (employee == null) {
	           log.error("Employee with id {} not found.", id);
	           return new ResponseEntity(
	                   new EmployeeError("Employee with id " + id + " not found."),
	                   HttpStatus.NOT_FOUND);
	       }
	       employeeService.deleteEmployeeById(id);
	       return new ResponseEntity<Employee>(employee, HttpStatus.OK);
	   }
    
    @RequestMapping(value = "/employee/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteEmployee(@PathVariable("id") int id) {
        log.info("Fetching & Deleting Employee with id {}", id);
        Employee employee = employeeService.findById(id);
        if (employee == null) {
            log.error("Unable to delete. Employee with id {} not found.", id);
            return new ResponseEntity(
                    new EmployeeError("Unable to delete. Employee with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        employeeService.deleteEmployeeById(id);
        return new ResponseEntity<Employee>(HttpStatus.NO_CONTENT);
    }

}
