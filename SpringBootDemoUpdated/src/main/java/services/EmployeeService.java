/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.util.List;
import models.Employee;

/**
 *
 * @author evita
 */
public interface EmployeeService {
    
   public List<Employee> findAllEmployees();
   void deleteEmployeeById(int id);
   Employee findById(int id);
    
}
