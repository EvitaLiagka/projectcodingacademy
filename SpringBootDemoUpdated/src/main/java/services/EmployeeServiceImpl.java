/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import models.Employee;
import org.springframework.stereotype.Service;

/**
 *
 * @author evita
 */

@Service("emmployeeService")
@Slf4j
public class EmployeeServiceImpl implements EmployeeService{
    
    private static List<Employee> employees;
    static{employees = populateDummyUsers();
    }
    
    private static List<Employee> populateDummyUsers(){
    List<Employee> employees = new ArrayList<>();
    employees.add(new Employee(1, "Sara", 12, 0));
    employees.add(new Employee(2, "Ev", 14, 4));
    return employees;
    }

    public List<Employee> findAllEmployees() {
    return employees;
    }

    public void deleteEmployeeById(int id) {
       employees.remove(id);
    }

    public Employee findById(int id) {
        return employees.get(id);
    }
}
