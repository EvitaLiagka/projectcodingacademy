/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.springexample.demo;

import java.util.LinkedHashMap;
import java.util.List;
import models.Employee;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import utils.EmployeeError;

/**
 *
 * @author evita
 */
public class SpringBootTestClient {

    public static final String REST_SERVICE_URI = "http://localhost:9085";

    private static void listAllEmployees() {
        System.out.println("Testing listAllEmployees API---------");

        RestTemplate restTemplate = new RestTemplate();
        List<LinkedHashMap<String, Object>> employeesMap
                = restTemplate.getForObject(REST_SERVICE_URI + "/employee/", List.class);
    }


private static void deleteEmployeeById() {
        System.out.println("Testing deleteEmployeeById API---------");

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(REST_SERVICE_URI + "/employee/1");
    }
//        if(employee == null){}

public static void main(String[] args) {
        listAllEmployees();
        deleteEmployeeById();
        listAllEmployees();
    }
}
