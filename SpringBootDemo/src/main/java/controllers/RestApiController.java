/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import models.Employee;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import services.EmployeeServiceImpl;


/**
 *
 * @author evita
 */
@RestController
@Slf4j
public class RestApiController {
    
    EmployeeServiceImpl employeeService = new EmployeeServiceImpl();

    //@Autowired
    //an instance
    @RequestMapping(value = "/testme", method = RequestMethod.GET)
    public ResponseEntity<String> testme() {
        String myreturn = "hi";
        return new ResponseEntity<String>(myreturn, HttpStatus.OK);
        
   
    }
    
    @RequestMapping(value = "/employee", method = RequestMethod.GET)
    public ResponseEntity<List<Employee>> listAllUsers(){
   List<Employee> employees = employeeService.findAllEmployees();
    if (employees.isEmpty()){
    return new ResponseEntity(HttpStatus.NO_CONTENT);}
    return new ResponseEntity<List<Employee>>(employees, HttpStatus.OK);}

}
