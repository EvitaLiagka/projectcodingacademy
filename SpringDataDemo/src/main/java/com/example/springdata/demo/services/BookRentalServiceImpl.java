/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.springdata.demo.services;

import com.example.springdata.demo.models.Book;
import com.example.springdata.demo.repositories.BookRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author evita
 */
public class BookRentalServiceImpl implements BookRentalService {

    @Autowired
    private BookRepository bookRepository;
    
    @Override
    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    @Override
    public String getAuthorByBookTitle(String title) {
        return null;
    }
    
}
