/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.springdata.demo.repositories;

import com.example.springdata.demo.models.Book;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author evita
 */
public interface BookRepository extends JpaRepository<Book,Long> {

    Book findByTitle (String title);
    
}
          