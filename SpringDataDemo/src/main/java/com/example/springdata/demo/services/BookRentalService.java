/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.springdata.demo.services;

import com.example.springdata.demo.models.Book;
import java.util.List;

/**
 *
 * @author evita
 */

public interface BookRentalService {

List<Book> getAllBooks();
String getAuthorByBookTitle(String title);

}
