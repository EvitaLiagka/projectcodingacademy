/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.springdata.demo.services;

import com.example.springdata.demo.models.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;
import com.example.springdata.demo.repositories.BookRepository;

/**
 *
 * @author evita
 */
@Service
public class DBInit implements ApplicationListener<ApplicationReadyEvent> {
//allos tropos na kaneis init me command line runner
//getters de dinei to data, dinei setters

    @Autowired
    private BookRepository bookRepository;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent e) {
        Book myAwesomeBook = new Book();
        myAwesomeBook.setNumberOfPages(570);
        myAwesomeBook.setAuthor("Very Hardcore Engineer");
        myAwesomeBook.setTitle("How to not curse while using Java");
        bookRepository.save(myAwesomeBook);

    }

}
