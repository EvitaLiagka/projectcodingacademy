package com.example.springdata.demo;

import com.example.springdata.demo.models.Book;
import com.example.springdata.demo.repositories.BookRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
//@DataJpaTest
@Slf4j
public class DemoApplicationTests {
    
    @Autowired
    private BookRepository bookRepository;
	@Test
	public void contextLoads() {
	}

    @Test
    public void findByTitle() {
        Book myAwesomeBook = new Book();
        myAwesomeBook.setNumberOfPages(570);
        myAwesomeBook.setAuthor("Very Hardcore Engineer");
        myAwesomeBook.setTitle("How to not curse while using Java");
        bookRepository.save(myAwesomeBook);
        
        log.info("Testing find by title");
        Book book =bookRepository.findByTitle("myAwesomeBook");
        log.info("Book found: ", book.toString());
    }

}
